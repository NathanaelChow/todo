"use strict";

Template.form.events({
    'submit .add-new-task': function (event) {
        event.preventDefault();
        
        var taskName = event.currentTarget.children[0].firstElementChild.value;
        
    if (!taskName.trim() ) {
            alert("Can't post empty task!");
    }
    else {
        
        Collections.Todo.insert({
            name: taskName,
            createdAt: new Date(),
            complete: false
        });
    }
        
        
        
        
        event.currentTarget.children[0].firstElementChild.value = "";
        return false;
    }
});

Template.todos.events({
   'click .delete-task': function (event) {
       Collections.Todo.remove({_id: this._id});
   },
    
    'click .complete-task': function (event) {
        Collections.Todo.update({_id: this._id}, {$set: {complete: true}});
    },
    'click .incomplete-task': function (event) {
        Collections.Todo.update({_id: this._id}, {$set: {complete: false}});
    },
    'click .edit-task': function (event) {
        var new_text= Collections.Todo.findOne({_id: this._id}, {name: 1}).name;
        var new_text=prompt(new_text,new_text);
        Collections.Todo.update({_id: this._id}, {$set: {name: new_text}});
        
    }
    
});



